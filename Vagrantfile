# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/precise32"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
    sudo add-apt-repository ppa:webupd8team/java
	sudo apt-get update
	sudo apt-get install -y oracle-java8-installer
	sudo apt-get install -y oracle-java8-set-default
	wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.1.deb
	dpkg -i elasticsearch-1.7.1.deb
	cd /usr/share/elasticsearch
	sudo su -c "bin/plugin -install mobz/elasticsearch-head" -s /bin/sh elasticsearch
  SHELL

  config.vm.provision "shell", inline: "sudo /etc/init.d/elasticsearch start", run: "always"

  # ES Server 1
  config.vm.define "es1" do |es1|
    es1.vm.provider "virtualbox" do |vb|
  	   # Display the VirtualBox GUI when booting the machine
  	   vb.gui = false
  	
  	   # Customize the amount of memory on the VM:
  	   vb.memory = "512"
  	end

  	es1.vm.network "private_network", ip: "192.168.50.10"

  	es1.vm.provision "shell", inline: <<-SHELL
		sudo echo "network.host: 192.168.50.10" >> /etc/elasticsearch/elasticsearch.yml
  	SHELL

  	es1.vm.provision "shell", inline: <<-SHELL
  		cd /vagrant
  		curl -XPOST 'localhost:9200/bank/account/_bulk?pretty' --data-binary @esBulk.json
  	SHELL
  end

  # ES Server 2
  config.vm.define "es2" do |es2|
    #web.vm.box = "apache"
    es2.vm.provider "virtualbox" do |vb|
  	   # Display the VirtualBox GUI when booting the machine
  	   vb.gui = false
  	
  	   # Customize the amount of memory on the VM:
  	   vb.memory = "512"
  	end

  	es2.vm.network "private_network", ip: "192.168.50.20"

  	es2.vm.provision "shell", inline: <<-SHELL
		sudo echo "network.host: 192.168.50.20" >> /etc/elasticsearch/elasticsearch.yml
  	SHELL

  end

end
